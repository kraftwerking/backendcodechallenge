## BackEndCodeChallenge

### Prerequisites for Building
* Java 1.8
* Gradle (for building)
* JUnit (for test dependencies, will be automatically downloaded by Gradle)

### Prerequisites for Executing the Program
* Java 1.8

### How to Build

```bash
cd /path/to/project-root
gradle build
```

This build task compiles the code, builds a jar file in `build/libs` directory, and executes the
tests.

### How to Run the Program

### Missing Letters
Change build.gradle to point to main class

`attributes 'Main-Class': 'com.kraftwerking.backendcodechallenge.MissingLettersMain'`

After `gradle build`:

```bash
java -jar /path/to/<your-jar>.jar "your string args"
```

### Missing Letters
Change build.gradle to point to main class

`attributes 'Main-Class': 'com.kraftwerking.backendcodechallenge.MissingLettersMain'`

After `gradle build`:

```bash
java -jar /path/to/<your-jar>.jar "your string args"


### TODO

* Expand javadoc
* Expand unit tests
* Expand acceptance tests

