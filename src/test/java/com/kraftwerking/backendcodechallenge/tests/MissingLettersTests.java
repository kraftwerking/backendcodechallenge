package com.kraftwerking.backendcodechallenge.tests;

import org.junit.Test;

import com.kraftwerking.backendcodechallenge.MissingLetters;

import static org.junit.Assert.*;

public class MissingLettersTests {
	MissingLetters missingLetters = new MissingLetters();
	
    @Test
    public void test1() {
    	String input = "A quick brown fox jumps over the lazy dog";
    	String output = missingLetters.findMissingLetters(input);
    	String expected = "";
    	assertTrue(output.equals(expected));
    }
    
    @Test
    public void test2() {
    	String input = "A slow yellow fox crawls under the proactive dog";
    	String output = missingLetters.findMissingLetters(input);
    	String expected = "bjkmqz";
    	assertTrue(output.equals(expected));
    }
    
    @Test
    public void test3() {
    	String input = "Lions, and tigers, and bears, oh my!";
    	String output = missingLetters.findMissingLetters(input);
    	String expected = "cfjkpquvwxz";
    	assertTrue(output.equals(expected));
    }
    
    @Test
    public void test4() {
    	String input = "";
    	String output = missingLetters.findMissingLetters(input);
    	String expected = "abcdefghijklmnopqrstuvwxyz";
    	assertTrue(output.equals(expected));
    }
}
