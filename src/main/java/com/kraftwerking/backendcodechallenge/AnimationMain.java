package com.kraftwerking.backendcodechallenge;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AnimationMain {
	
	private static final Logger logger = LoggerFactory.getLogger(AnimationMain.class);
	private static Animation animation = new Animation();
	
	public static void main(String args[]){
        if (args.length < 1) {
            logger.error("Input String must be supplied to this program.");
            System.exit(1);
        }

        String init = args[0];
		Pattern pattern = Pattern.compile(", *");
		Matcher matcher = pattern.matcher(init);
		String string1 = null;
		String string2 = null;

		// get speed, chamber from input
		if (matcher.find()) {
			string1 = init.substring(0, matcher.start());
			string2 = init.substring(matcher.end());
		}
		int speed = Integer.parseInt(string1);
		
        //logger.info(s);
        String[] result = animation.animate(speed, string2);
        for(String s:result){
        	System.out.println(s);
        }

	}

}

