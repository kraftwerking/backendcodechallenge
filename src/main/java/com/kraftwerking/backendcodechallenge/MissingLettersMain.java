package com.kraftwerking.backendcodechallenge;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MissingLettersMain {
	
	private static final Logger logger = LoggerFactory.getLogger(MissingLettersMain.class);
	private static MissingLetters missingLetters = new MissingLetters();
	
	public static void main(String args[]){
        if (args.length < 1) {
            logger.error("Input String must be supplied to this program.");
            System.exit(1);
        }

        String s = args[0];
        logger.info(missingLetters.findMissingLetters(s));
        System.exit(1);

	}

}

