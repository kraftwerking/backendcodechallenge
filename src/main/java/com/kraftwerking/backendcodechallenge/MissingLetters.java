package com.kraftwerking.backendcodechallenge;

import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MissingLetters {
	private static final Logger logger = LoggerFactory.getLogger(MissingLetters.class);

	public String findMissingLetters(String s) {
		char[] input = s.toLowerCase().toCharArray();

		HashSet<Character> uniqueCharSet = new HashSet<Character>();
		// remove non alphabetic characters, alphabetize, unique chars
		for (Character eachChar : input) {
			if (Character.isLetter(eachChar)) {
				uniqueCharSet.add(eachChar);
			}
		}

		logger.info(uniqueCharSet.toString());
		char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		//String containing missing letters
		StringBuilder output = new StringBuilder();

		for (Character letter : alphabet) {
			if (!uniqueCharSet.contains(letter)) {
				logger.info(letter + " not found");
				output.append(letter);
			}
		}

		return output.toString();
	}
}
