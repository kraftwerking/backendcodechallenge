package com.kraftwerking.backendcodechallenge;

import java.util.ArrayList;
import java.util.List;

public class Animation {
	
	public String[] animate(int speed, String init){
	    List<String> output = new ArrayList<String>();
	    output.add(init.replaceAll("[ RL]", "X"));
	 
	        /* First set the particle position in array using init String and write it to output */
	        char[] currLine = init.toCharArray();
	 
		while (!checkAndSetValues(currLine, output)){
	 
	            //Every time create a new line and put the corresponding characters at required places.
	            char[] nextLine = new char[currLine.length];
	 
	            for(int i = 0; i < currLine.length; i++){
	                if(currLine[i] == 'L' && i-speed > 0){
	                    nextLine[i-speed] = 'L';
	                } else if(currLine[i] == 'R' && i+speed < currLine.length){
	                    nextLine[i+speed] = 'R';
	                }
	            }
	            currLine = nextLine;
	        }

	 
	    return output.toArray(new String[output.size()]);
	}
	
	private boolean checkAndSetValues(final char[] particleArr, List<String> output){
	    boolean isParticleOut = true;
	    StringBuilder outputBuf = new StringBuilder();
	    //Create the output string by checking the character 0 which is the default char array value.
	    for(int i = 0; i < particleArr.length; i++){
	        if(particleArr[i] != Character.MIN_VALUE){
	        	System.out.println(particleArr[i]);
	            outputBuf.append('X');
	            isParticleOut = false;
	        }else{
	            outputBuf.append('.');
	        }
	    }
	    output.add(outputBuf.toString());
	    return isParticleOut;
	}

}
